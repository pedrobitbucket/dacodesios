# Prueba iOS
Este repositorio contiene el proyecto correspondiente a la [prueba de iOS](https://bitbucket.org/dacodes/pruebas/src/master/iOS/) de DaCodes.

## Instrucciones
Para poder correr el proyecto se deben seguir los siguientes pasos:

1. Clonar el proyecto en su equipo.
  ```
  git clone https://pedrobitbucket@bitbucket.org/pedrobitbucket/dacodesios.git
  ```
2. Abrir una terminar y navegar hasta la carpeta del proyecto
3. Cerrar el xcode antes de ejecutar el comando:
  ```
  pod install
  ```
  Si no se tiene instalado cocoa pods, seguir este [tutorial](https://guides.cocoapods.org/using/getting-started.html)
4. Abrir el workspace en vez del proyecto.
5. Ejecutar la aplicacion en el simulador o en algun dispositivo.

Para cualquier duda mandar un corre a pedro128@gmail.com
