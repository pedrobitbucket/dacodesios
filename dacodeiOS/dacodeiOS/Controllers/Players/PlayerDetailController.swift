//
//  PlayerDetailController.swift
//  dacodeiOS
//
//  Created by Pedro Romero on 7/30/18.
//  Copyright © 2018 Predrusky. All rights reserved.
//

import UIKit

class PlayerDetailController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var player: Player?
    var menuOptions = [[String: Any]]()

    override func viewDidLoad() {
        super.viewDidLoad()
        let profile = ["title": "", "type": 0] as [String : Any]
        let birthday = ["title": "FECHA DE NACIMIENTO", "type": 1] as [String : Any]
        let birthplace = ["title": "LUGAR DE NACIMIENTO", "type": 2] as [String : Any]
        let weight = ["title": "PESO", "type": 3] as [String : Any]
        let height = ["title": "ALTURA", "type": 4] as [String : Any]
        let lastTeam = ["title": "EQUIPO ANTERIOR", "type": 5] as [String : Any]
        
        self.menuOptions.append(profile)
        self.menuOptions.append(birthday)
        self.menuOptions.append(birthplace)
        self.menuOptions.append(weight)
        self.menuOptions.append(height)
        self.menuOptions.append(lastTeam)
    }
    
    // MARK: - IBAction
    @IBAction func handleTap(recognizer:UITapGestureRecognizer) {
        let overlay = self.view.viewWithTag(10)
        let container = self.view.viewWithTag(20)!
        
        UIView.animate(withDuration: 0.4, animations: {
            overlay?.backgroundColor = UIColor.black.withAlphaComponent(0.0)
            var initFrame = container.frame
            initFrame.origin.y += container.frame.size.height
            container.frame = initFrame
        }) { (finished) in
            self.dismiss(animated: false, completion: nil)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - UITableViewDataSource
extension PlayerDetailController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menuOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let option = self.menuOptions[indexPath.row]
        let type = option["type"] as! Int
        switch type {
        case 0: // Profile
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerDetailProfileCell", for: indexPath) as! PlayerDetailProfileCell
            if let player = self.player {
                cell.player = player
            }
            return cell
        case 1: // birthday
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerDetailOptionCell", for: indexPath) as! PlayerDetailOptionCell
            cell.playerTitle.text = option["title"] as? String
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            cell.playerValue.text = formatter.string(from: (self.player?.birthday)!)
            return cell
        case 2: // birthplace
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerDetailOptionCell", for: indexPath) as! PlayerDetailOptionCell
            cell.playerTitle.text = option["title"] as? String
            cell.playerValue.text = self.player?.birth_place
            return cell
        case 3: // weight
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerDetailOptionCell", for: indexPath) as! PlayerDetailOptionCell
            cell.playerTitle.text = option["title"] as? String
            cell.playerValue.text = "\(self.player!.weight) kg"
            return cell
        case 4: // height
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerDetailOptionCell", for: indexPath) as! PlayerDetailOptionCell
            cell.playerTitle.text = option["title"] as? String
            cell.playerValue.text = "\(self.player!.height) m"
            return cell
        case 5: // lastTeam
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerDetailOptionCell", for: indexPath) as! PlayerDetailOptionCell
            cell.playerTitle.text = option["title"] as? String
            cell.playerValue.text = self.player?.last_team
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerDetailOptionCell", for: indexPath) as! PlayerDetailOptionCell
            return cell
            
        }
    }
}
