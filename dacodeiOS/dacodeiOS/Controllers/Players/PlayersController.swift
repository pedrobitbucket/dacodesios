//
//  PlayersController.swift
//  dacodeiOS
//
//  Created by Pedro Romero on 7/30/18.
//  Copyright © 2018 Predrusky. All rights reserved.
//

import UIKit

class PlayersController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    var players = [Player]()
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.refreshControl = refreshControl
        self.refreshControl.addTarget(self, action: #selector(refreshPlayersData(_:)), for: .valueChanged)
        self.getAllPlayers()
    }
    
    // MARK: - Private
    private func getAllPlayers() {
        Player.all(completion: { (error, players) in
            if let players = players {
                self.players = players as! [Player]
                self.collectionView.reloadData()
            }
            self.refreshControl.endRefreshing()
        })
    }
    
    @objc private func refreshPlayersData(_ sender: Any) {
        self.getAllPlayers()
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "seguePlayerDetail" {
            let cell = sender as! PlayerCollectionCell
            let playerDetailController = segue.destination as! PlayerDetailController
            playerDetailController.player = cell.player
        }
    }

}

// MARK: - UICollectionViewDataSource
extension PlayersController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.players.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlayerCollectionCell", for: indexPath) as! PlayerCollectionCell
        let player = self.players[indexPath.row]
        cell.player = player
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension PlayersController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let itemSize = flowLayout.itemSize
        let width = (collectionView.frame.size.width - flowLayout.minimumLineSpacing - flowLayout.sectionInset.left)/3
        let height = (itemSize.height*width)/itemSize.width
        return CGSize(width: width, height: height)
    }
}
