//
//  StatisticsController.swift
//  dacodeiOS
//
//  Created by Pedro Romero on 7/30/18.
//  Copyright © 2018 Predrusky. All rights reserved.
//

import UIKit

class StatisticsController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var statistics = [Statistic]()
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.refreshControl = refreshControl
        self.refreshControl.addTarget(self, action: #selector(refreshStatisticsData(_:)), for: .valueChanged)
        self.getAllStatistics()
    }
    
    // MARK: - Private
    private func getAllStatistics() {
        Statistic.all(completion: { (error, statistics) in
            if let statistics = statistics {
                self.statistics = statistics as! [Statistic]
                self.tableView.reloadData()
            }
            self.refreshControl.endRefreshing()
        })
    }
    
    @objc private func refreshStatisticsData(_ sender: Any) {
        self.getAllStatistics()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - UITableViewDataSource
extension StatisticsController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.statistics.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StatisticCell") as! StatisticCell
        let statistic = self.statistics[indexPath.row]
        cell.statistic = statistic
        return cell
    }
}

// MARK: - UITableViewDelegate
extension StatisticsController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! HeaderCell
        return headerCell
    }
}
