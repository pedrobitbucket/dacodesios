//
//  MenuController.swift
//  dacodeiOS
//
//  Created by Pedro Romero on 7/30/18.
//  Copyright © 2018 Predrusky. All rights reserved.
//

import UIKit
import SideMenu

class MenuController: UITableViewController {
    var menuOptions = [[String: Any]]()

    override func viewDidLoad() {
        super.viewDidLoad()
        SideMenuManager.default.menuPushStyle = .popWhenPossible
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        
        let menuProfileOption: [String: Any] = ["userImage": "VENlogo", "userName": "Venados F.C.", "userEmail": "pedro128@gmail.com", "type":2]
        let menuOption1: [String: Any] = ["title": "Inicio", "segueStoryboardId":"segueHome", "type": 1, "icon": "menu_home"]
        let menuOption2: [String: Any] = ["title": "Estadísticas", "segueStoryboardId":"segueStatistics", "type": 1, "icon": "menu_statistics"]
        let menuOption3: [String: Any] = ["title": "Jugadores", "segueStoryboardId":"seguePlayers", "type": 1, "icon": "menu_players"]
        
        self.menuOptions.append(menuProfileOption)
        self.menuOptions.append(menuOption1)
        self.menuOptions.append(menuOption2)
        self.menuOptions.append(menuOption3)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.menuOptions.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let menuOption = self.menuOptions[indexPath.row]
        let type = menuOption["type"] as! Int
        
        switch type {
        case 2: // ProfileCell
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
            cell.profileName.text = menuOption["userName"] as! String?
            cell.profileImage.image = UIImage(named: menuOption["userImage"] as! String)
            return cell
        case 1: //MenuCell
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuCell
            cell.menuIcon.image = UIImage(named: menuOption["icon"] as! String)
            cell.menuName.text = menuOption["title"] as! String?
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath)
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menuOption = self.menuOptions[indexPath.row]
        
        if let segueStoryboardId = menuOption["segueStoryboardId"] {
            self.performSegue(withIdentifier: segueStoryboardId as! String, sender: nil)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
