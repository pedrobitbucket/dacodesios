//
//  HomeController.swift
//  dacodeiOS
//
//  Created by Pedro Romero on 7/29/18.
//  Copyright © 2018 Predrusky. All rights reserved.
//

import UIKit

class HomeController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var copaBtn: UIButton!
    @IBOutlet weak var ascensoBtn: UIButton!
    var games = [Game]()
    var copaMeses = [Int]()
    var copa = [Game]()
    var ascensoMeses = [Int]()
    var ascenso = [Game]()
    var currentLeague = 1
    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.refreshControl = refreshControl
        self.refreshControl.addTarget(self, action: #selector(refreshGamesData(_:)), for: .valueChanged)
        self.getAllGames()
    }
    
    // MARK: - Private
    private func getAllGames() {
        Game.all(completion: { (error, games) in
            if let games = games {
                self.games = games as! [Game]
                self.filterGames()
                self.tableView.reloadData()
            }
            self.refreshControl.endRefreshing()
        })
    }
    
    @objc private func refreshGamesData(_ sender: Any) {
        self.getAllGames()
    }

    
    private func filterGames() {
        self.copa = [Game]()
        self.ascenso = [Game]()
        self.copaMeses = [Int]()
        self.ascensoMeses = [Int]()
        for game in self.games {
            let calendar = Calendar(identifier: .gregorian)
            let components = calendar.dateComponents([.month], from: game.datetime!)
            let month = components.month
            if game.league == "Ascenso MX" {
                self.ascenso.append(game)
                if !self.ascensoMeses.contains(month!) {
                    self.ascensoMeses.append(month!)
                }
            } else if game.league == "Copa MX" {
                self.copa.append(game)
                if !self.copaMeses.contains(month!) {
                    self.copaMeses.append(month!)
                }
            }
        }
    }
    
    private func filterGamesBy(month: Int) -> [Game] {
        var gamesByMonth = [Game]()
        let calendar = Calendar(identifier: .gregorian)
        if self.currentLeague == 1 { // Copa MX
            for game in self.copa {
                let components = calendar.dateComponents([.month], from: game.datetime!)
                let gameMonth = components.month
                if month == gameMonth {
                    gamesByMonth.append(game)
                }
            }
        } else { // Ascenso MX
            for game in self.ascenso {
                let components = calendar.dateComponents([.month], from: game.datetime!)
                let gameMonth = components.month
                if month == gameMonth {
                    gamesByMonth.append(game)
                }
            }
        }
        return gamesByMonth
    }
    
    // MARK: - IBActions
    @IBAction func copaAction(_ sender: UIButton) {
        self.copaBtn.backgroundColor = UIColor.clear
        self.ascensoBtn.backgroundColor = UIColor.white
        self.currentLeague = 1
        self.tableView.reloadData()
    }
    
    @IBAction func ascensoAction(_ sender: UIButton) {
        self.copaBtn.backgroundColor = UIColor.white
        self.ascensoBtn.backgroundColor = UIColor.clear
        self.currentLeague = 2
        self.tableView.reloadData()
    }
    
    @IBAction func addGameCalendarAction(_ sender: UIButton) {
        let buttonPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPoint) {
            let eventHelper = EventHelper()
            if self.currentLeague == 1 { // Copa MX
                let month = self.copaMeses[indexPath.section]
                let games = self.filterGamesBy(month: month)
                let game = games[indexPath.row]
                eventHelper.addEventFor(game: game)
            } else { // Ascenso MX
                let month = self.ascensoMeses[indexPath.section]
                let games = self.filterGamesBy(month: month)
                let game = games[indexPath.row]
                eventHelper.addEventFor(game: game)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - UITableViewDataSource
extension HomeController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.currentLeague == 1 { // Copa MX
            return self.copaMeses.count
        } else { // Ascenso MX
            return self.ascensoMeses.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.currentLeague == 1 { // Copa MX
            let month = self.copaMeses[section]
            let games = self.filterGamesBy(month: month)
            return games.count
        } else { // Ascenso MX
            let month = self.ascensoMeses[section]
            let games = self.filterGamesBy(month: month)
            return games.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GameCell") as! GameCell
        if self.currentLeague == 1 { // Copa MX
            let month = self.copaMeses[indexPath.section]
            let games = self.filterGamesBy(month: month)
            let game = games[indexPath.row]
            cell.game = game
        } else { // Ascenso MX
            let month = self.ascensoMeses[indexPath.section]
            let games = self.filterGamesBy(month: month)
            let game = games[indexPath.row]
            cell.game = game
        }
        return cell
    }
}

// MARK: - UITableViewDelegate
extension HomeController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! HeaderCell
        if self.currentLeague == 1 { // Copa MX
            let month = self.copaMeses[section]
            headerCell.month = month
        } else { // Ascenso MX
            let month = self.ascensoMeses[section]
            headerCell.month = month
        }
        return headerCell
    }
}
