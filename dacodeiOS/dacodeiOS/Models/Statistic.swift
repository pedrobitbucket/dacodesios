//
//  Statistic.swift
//  dacodeiOS
//
//  Created by Pedro Romero on 7/30/18.
//  Copyright © 2018 Predrusky. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Statistic: NSObject {
    var position: Int = 0
    var image: String = ""
    var team: String = ""
    var games: Int = 0
    var win: Int = 0
    var loss: Int = 0
    var f_goals: Int = 0
    var a_goals: Int = 0
    var score_diff: Int = 0
    var points: Int = 0
    var efec: Int = 0
    
    init(json: JSON) {
        super.init()
        if let position = json["position"].int {
            self.position = position
        }
        if let image = json["image"].string {
            self.image = image
        }
        if let team = json["team"].string {
            self.team = team
        }
        if let games = json["games"].int {
            self.games = games
        }
        if let win = json["win"].int {
            self.win = win
        }
        if let loss = json["loss"].int {
            self.loss = loss
        }
        if let f_goals = json["f_goals"].int {
            self.f_goals = f_goals
        }
        if let a_goals = json["a_goals"].int {
            self.a_goals = a_goals
        }
        if let score_diff = json["score_diff"].int {
            self.score_diff = score_diff
        }
        if let points = json["points"].int {
            self.points = points
        }
        if let efec = json["efec"].int {
            self.efec = efec
        }
    }
}

// MARK: - Route
enum StatisticRouter: URLRequestConvertible {
    case all()
    
    var method: HTTPMethod {
        switch self {
        case .all:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .all:
            return "statistics"
        }
    }
    
    // MARK: URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let urlRequest = try URLRequest(url: APIManager.basURL + path, method: method)
        return urlRequest
    }
}

// MARK: - API Calls
extension Statistic {
    typealias completionRequest = (_ error: Error?, _ object: Any?) -> Void
    
    static func all(completion: @escaping completionRequest) {
        APIManager.shared.callAPI(urlRequest: StatisticRouter.all()) { (error, object) in
            if error != nil {
                print("error: \(String(describing: error?.localizedDescription))")
                completion(error, nil)
            } else {
                var statistics = [Statistic]()
                let json = JSON(object!)
                if let statisticsJson = json["data"]["statistics"].array {
                    for statisticJson in statisticsJson {
                        statistics.append(Statistic(json: statisticJson))
                    }
                }
                completion(nil, statistics)
            }
        }
    }
}
