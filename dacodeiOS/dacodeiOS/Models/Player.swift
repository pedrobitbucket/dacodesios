//
//  Player.swift
//  dacodeiOS
//
//  Created by Pedro Romero on 7/30/18.
//  Copyright © 2018 Predrusky. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Player: NSObject {
    var name: String = ""
    var first_surname: String = ""
    var second_surname: String = ""
    var birthday: Date?
    var birth_place: String = ""
    var weight: Double = 0
    var height: Double = 0
    var position: String = ""
    var number: Int = 0
    var position_short: String = ""
    var last_team: String = ""
    var image: String = ""
    
    init(json: JSON) {
        super.init()
        if let name = json["name"].string {
            self.name = name
        }
        if let first_surname = json["first_surname"].string {
            self.first_surname = first_surname
        }
        if let second_surname = json["second_surname"].string {
            self.second_surname = second_surname
        }
        if let birthdayString = json["birthday"].string {
            let dateFormatter = ISO8601DateFormatter()
            let datetimeDate = dateFormatter.date(from: birthdayString)
            self.birthday = datetimeDate
        }
        if let birth_place = json["birth_place"].string {
            self.birth_place = birth_place
        }
        if let weight = json["weight"].double {
            self.weight = weight
        }
        if let height = json["height"].double {
            self.height = height
        }
        if let position = json["position"].string {
            self.position = position
        }
        if let number = json["number"].int {
            self.number = number
        }
        if let position_short = json["position_short"].string {
            self.position_short = position_short
        }
        if let last_team = json["last_team"].string {
            self.last_team = last_team
        }
        if let image = json["image"].string {
            self.image = image
        }
    }
}

// MARK: - Route
enum PlayerRouter: URLRequestConvertible {
    case all()
    
    var method: HTTPMethod {
        switch self {
        case .all:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .all:
            return "players"
        }
    }
    
    // MARK: URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let urlRequest = try URLRequest(url: APIManager.basURL + path, method: method)
        return urlRequest
    }
}

// MARK: - API Calls
extension Player {
    typealias completionRequest = (_ error: Error?, _ object: Any?) -> Void
    
    static func all(completion: @escaping completionRequest) {
        APIManager.shared.callAPI(urlRequest: PlayerRouter.all()) { (error, object) in
            if error != nil {
                print("error: \(String(describing: error?.localizedDescription))")
                completion(error, nil)
            } else {
                var players = [Player]()
                let json = JSON(object!)
                if let fordwardsJson = json["data"]["team"]["forwards"].array {
                    for fordwardJson in fordwardsJson {
                        players.append(Player(json: fordwardJson))
                    }
                }
                if let centersJson = json["data"]["team"]["centers"].array {
                    for centerJson in centersJson {
                        players.append(Player(json: centerJson))
                    }
                }
                if let defensesJson = json["data"]["team"]["defenses"].array {
                    for defensJson in defensesJson {
                        players.append(Player(json: defensJson))
                    }
                }
                if let goalkeepersJson = json["data"]["team"]["goalkeepers"].array {
                    for goalkeeperJson in goalkeepersJson {
                        players.append(Player(json: goalkeeperJson))
                    }
                }
                if let coachesJson = json["data"]["team"]["coaches"].array {
                    for coacheJson in coachesJson {
                        players.append(Player(json: coacheJson))
                    }
                }
                completion(nil, players)
            }
        }
    }
}
