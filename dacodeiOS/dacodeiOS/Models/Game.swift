//
//  Game.swift
//  dacodeiOS
//
//  Created by Pedro Romero on 7/29/18.
//  Copyright © 2018 Predrusky. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Game: NSObject {
    var local: Bool = false
    var opponent: String = ""
    var opponent_image: String = ""
    var datetime: Date?
    var league: String = ""
    var image: String = ""
    var home_score: Int = 0
    var away_score: Int = 0
    
    init(json: JSON) {
        super.init()
        if let local = json["local"].bool {
            self.local = local
        }
        if let opponent = json["opponent"].string {
            self.opponent = opponent
        }
        if let opponent_image = json["opponent_image"].string {
            self.opponent_image = opponent_image
        }
        if let datetime = json["datetime"].string {
            let dateFormatter = ISO8601DateFormatter()
            let datetimeDate = dateFormatter.date(from:datetime)
            self.datetime = datetimeDate
        }
        if let league = json["league"].string {
            self.league = league
        }
        if let image = json["image"].string {
            self.image = image
        }
        if let home_score = json["home_score"].int {
            self.home_score = home_score
        }
        if let away_score = json["away_score"].int {
            self.away_score = away_score
        }
    }
}

// MARK: - Route
enum GameRouter: URLRequestConvertible {
    case all()
    
    var method: HTTPMethod {
        switch self {
        case .all:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .all:
            return "games"
        }
    }
    
    // MARK: URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let urlRequest = try URLRequest(url: APIManager.basURL + path, method: method)
        return urlRequest
    }
}

// MARK: - API Calls
extension Game {
    typealias completionRequest = (_ error: Error?, _ object: Any?) -> Void
    
    static func all(completion: @escaping completionRequest) {
        APIManager.shared.callAPI(urlRequest: GameRouter.all()) { (error, object) in
            if error != nil {
                print("error: \(String(describing: error?.localizedDescription))")
                completion(error, nil)
            } else {
                var games = [Game]()
                let json = JSON(object!)
                if let gamesJson = json["data"]["games"].array {
                    for gameJson in gamesJson {
                        games.append(Game(json: gameJson))
                    }
                }
                completion(nil, games)
            }
        }
    }
}
