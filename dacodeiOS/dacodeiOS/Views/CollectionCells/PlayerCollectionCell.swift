//
//  PlayerCollectionCell.swift
//  dacodeiOS
//
//  Created by Pedro Romero on 7/30/18.
//  Copyright © 2018 Predrusky. All rights reserved.
//

import UIKit

class PlayerCollectionCell: UICollectionViewCell {
    @IBOutlet weak var cardView: CardView!
    @IBOutlet weak var playerImage: UIImageView!
    @IBOutlet weak var position: UILabel!
    @IBOutlet weak var name: UILabel!
    
    var player: Player? {
        didSet {
            self.updateUI()
        }
    }
    
    // MARK: - Private
    private func updateUI() {
        if let player = self.player {
            self.playerImage.setImage(url: URL(string: player.image)!)
            self.position.text = player.position
            self.name.text = player.name
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.cardView.cornerRadius = self.cardView.frame.size.width/2
        self.playerImage.layer.cornerRadius = self.cardView.frame.size.width/2
    }
    
}
