//
//  StatisticCell.swift
//  dacodeiOS
//
//  Created by Pedro Romero on 7/30/18.
//  Copyright © 2018 Predrusky. All rights reserved.
//

import UIKit

class StatisticCell: UITableViewCell {
    @IBOutlet weak var position: UILabel!
    @IBOutlet weak var teamImage: UIImageView!
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var jjNumber: UILabel!
    @IBOutlet weak var dgNumber: UILabel!
    @IBOutlet weak var ptsNumber: UILabel!
    var statistic: Statistic? {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // MARK: - Private
    private func updateUI() {
        if let statistic = self.statistic {
            self.position.text = "\(statistic.position)"
            self.teamImage.setImage(url: URL(string: statistic.image)!)
            self.teamName.text = statistic.team
            self.jjNumber.text = "\(statistic.games)"
            self.dgNumber.text = "\(statistic.score_diff)"
            self.ptsNumber.text = "\(statistic.points)"
        }
    }
}
