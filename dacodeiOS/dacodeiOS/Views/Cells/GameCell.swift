//
//  GameCell.swift
//  dacodeiOS
//
//  Created by Pedro Romero on 7/29/18.
//  Copyright © 2018 Predrusky. All rights reserved.
//

import UIKit
import Imaginary

class GameCell: UITableViewCell {
    @IBOutlet weak var homeLogo: UIImageView!
    @IBOutlet weak var opponentLogo: UIImageView!
    @IBOutlet weak var homeName: UILabel!
    @IBOutlet weak var opponentName: UILabel!
    @IBOutlet weak var score: UILabel!
    @IBOutlet weak var dayNumber: UILabel!
    @IBOutlet weak var dayLetters: UILabel!
    let weekdaysName = ["DOM", "LUN", "MAR", "MIÉ", "JUE", "VIE", "SÁB"]
    var game: Game? {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Private
    private func updateUI() {
        if let game = self.game {
            self.opponentName.text = game.opponent
            self.score.text = "\(game.home_score)-\(game.away_score)"
            var calendar = Calendar(identifier: .gregorian)
            calendar.timeZone = TimeZone(abbreviation: "UTC")!
            let components = calendar.dateComponents([.weekday, .weekdayOrdinal, .day], from: game.datetime!)
            let weekday = components.weekday! - 1
            let day = components.day
            self.dayNumber.text = "\(day!)"
            self.dayLetters.text = self.weekdaysName[weekday]
            self.opponentLogo.setImage(url: URL(string: game.opponent_image)!)
        }
    }

}
