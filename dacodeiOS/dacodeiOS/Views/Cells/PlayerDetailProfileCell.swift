//
//  PlayerDetailProfileCell.swift
//  dacodeiOS
//
//  Created by Pedro Romero on 7/30/18.
//  Copyright © 2018 Predrusky. All rights reserved.
//

import UIKit

class PlayerDetailProfileCell: UITableViewCell {
    @IBOutlet weak var cardView: CardView!
    @IBOutlet weak var playerImage: UIImageView!
    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var playerPosition: UILabel!
    var player: Player? {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.cardView.cornerRadius = self.cardView.frame.size.width/2
        self.playerImage.layer.cornerRadius = self.cardView.frame.size.width/2
    }
    
    //MARK: - Private
    private  func updateUI() {
        if let player = self.player {
            self.playerImage.setImage(url: URL(string: player.image)!)
            self.playerPosition.text = player.position
            self.playerName.text = player.name
        }
    }

}
