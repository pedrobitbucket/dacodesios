//
//  EventHelper.swift
//  dacodeiOS
//
//  Created by Pedro Romero on 7/30/18.
//  Copyright © 2018 Predrusky. All rights reserved.
//

import UIKit
import EventKit

class EventHelper {
    let eventStore = EKEventStore()
    
    func generateEvent() -> Bool{
        let status = EKEventStore.authorizationStatus(for: EKEntityType.event)
        
        switch (status) {
        case EKAuthorizationStatus.notDetermined:
            // This happens on first-run
            requestAccessToCalendar()
        case EKAuthorizationStatus.authorized:
            // User has access
            print("User has access to calendar")
            return true
            //self.addAppleEvents()
        case EKAuthorizationStatus.restricted, EKAuthorizationStatus.denied:
            // We need to help them give us permission
            noPermission()
        }
        return false
    }
    
    func noPermission() {
        print("User has to change settings...goto settings to view access")
        let alert = UIAlertController(title: "Calendario", message: "No se tiene permiso para poder guardar el partido en el calendario. Habilita la opción de calendario en los ajustes de la aplicación.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ir a ajustes", style: .default, handler: { (action) -> Void in
            UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
        }))
        alert.addAction(UIAlertAction(title: "Aceptar", style: .cancel, handler: nil))
        alert.show()
    }
    
    func requestAccessToCalendar() {
        eventStore.requestAccess(to: .event, completion: { (granted, error) in
            if (granted) && (error == nil) {
                DispatchQueue.main.async {
                    print("User has access to calendar")
                    //self.addAppleEvents()
                }
            } else {
                DispatchQueue.main.async{
                    self.noPermission()
                }
            }
        })
    }
    
    func addEventFor(game: Game) {
        if self.generateEvent() {
            let event = EKEvent(eventStore: eventStore)
            event.title = "\(game.league) Venados F.C. vs \(game.opponent)"
            event.startDate = game.datetime
            event.endDate = game.datetime?.addingTimeInterval(TimeInterval(3.0 * 60.0 * 60.0))
            event.calendar = eventStore.defaultCalendarForNewEvents
            // 1800 seconds before = 30 mins
            let alarm:EKAlarm = EKAlarm(relativeOffset: -1800)
            event.alarms = [alarm]
            
            do {
                try eventStore.save(event, span: .thisEvent)
                print("events added with dates:")
            } catch let e as NSError {
                print(e.description)
                return
            }
            
            let message = "El partido: \(game.league) Venados F.C. vs \(game.opponent), se guardo en su calendario."
            self.showMessage(message: message, date: game.datetime!)
        }
    }
    
    private func showMessage(message: String, date: Date) {
        let alert = UIAlertController(title: "Calendario", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ir al calendario", style: .default, handler: { (action) -> Void in
            let interval = date.timeIntervalSinceReferenceDate
            let url = URL(string: "calshow:\(interval)")!
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }))
        alert.addAction(UIAlertAction(title: "Aceptar", style: .cancel, handler: nil))
        alert.show()
    }
}
