//
//  FilterModalSegue.swift
//  restaurants
//
//  Created by Pedro Romero on 6/30/18.
//  Copyright © 2018 Predrusky. All rights reserved.
//

import UIKit

class FilterModalSegue: UIStoryboardSegue {
    
    override func perform() {
        let toViewController = self.destination
        let fromViewController = self.source
        let containerView = UIApplication.shared.keyWindow
        
        let overlay = toViewController.view.viewWithTag(10)
        let tableView = toViewController.view.viewWithTag(20)
        
        overlay?.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        var initFrame = tableView!.frame
        initFrame.origin.y += tableView!.frame.size.height
        tableView!.frame = initFrame
        
        containerView?.insertSubview(toViewController.view, aboveSubview: fromViewController.view)
        
        UIView.animate(withDuration: 0.4, animations: {
            var newFrame = tableView!.frame
            newFrame.origin.y = UIScreen.main.bounds.size.height - newFrame.size.height
            tableView!.frame = newFrame
            overlay?.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        }) { (success) in
            fromViewController.present(toViewController, animated: false, completion: nil)
        }
    }
}
