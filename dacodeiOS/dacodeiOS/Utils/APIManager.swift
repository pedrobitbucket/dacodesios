//
//  APIManager.swift
//  restaurants
//
//  Created by Pedro Romero on 3/27/18.
//  Copyright © 2018 Predrusky. All rights reserved.
//

import Foundation
import Alamofire

class APIManager {
    typealias completionRequest = (_ error: Error?, _ object: Any?) -> Void
    static let shared = APIManager()
    static let basURL = "https://venados.dacodes.mx/api/"
    
    private init() {}
    
    public func callAPI(urlRequest: URLRequestConvertible, completion: @escaping completionRequest) {
        guard var urlRequest = urlRequest.urlRequest else { return }
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        print("request: \(String(describing: urlRequest.url?.absoluteString))")
        Alamofire.request(urlRequest)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    completion(nil, response.result.value)
                case .failure(let error):
                    completion(error, nil)
                }
        }
    }
}
