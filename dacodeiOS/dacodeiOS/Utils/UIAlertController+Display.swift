//
//  UIAlertController+Display.swift
//  dacodeiOS
//
//  Created by Pedro Romero on 7/30/18.
//  Copyright © 2018 Predrusky. All rights reserved.
//

import Foundation
import UIKit

extension UIAlertController {
    
    func show() {
        present(true, completion: nil)
    }
    
    func present(_ animated: Bool, completion: (() -> Void)?) {
        for window in UIApplication.shared.windows {
            if window.isMember(of: UIWindow.classForCoder()) {
                if let rootVC = window.rootViewController {
                    presentFromController(rootVC, animated: animated, completion: completion)
                }
            }
        }
    }
    
    fileprivate func presentFromController(_ controller: UIViewController, animated: Bool, completion: (() -> Void)?) {
        if  let navVC = controller as? UINavigationController,
            let visibleVC = navVC.visibleViewController {
            presentFromController(visibleVC, animated: animated, completion: completion)
        } else {
            if  let tabVC = controller as? UITabBarController,
                let selectedVC = tabVC.selectedViewController {
                presentFromController(selectedVC, animated: animated, completion: completion)
            } else {
                controller.present(self, animated: animated, completion: completion)
            }
        }
    }
}
